---
apiVersion: v1
kind: Template
metadata:
  creationTimestamp: null
  name: petclinic
objects:
  - apiVersion: v1
    kind: ServiceAccount
    metadata:
      labels:
        app: "${app_name}"
        project: "${project_name}"    
      creationTimestamp: null
      name: alm-totta
  - apiVersion: v1
    kind: DeploymentConfig
    metadata:
      name: tomcat8
      labels:
        app: "${app_name}"
        project: "${project_name}"
    spec:
      replicas: 1
      selector:
        deploymentConfig: tomcat8
      strategy:
        type: Recreate
      template:
        metadata:
          labels:
            deploymentConfig: 'tomcat8'
        spec:
          serviceAccountName: alm-totta
          serviceAccount: alm-totta
          terminationGracePeriodSeconds: 60
          containers:
            - capabilities: {}
              env:
                - name: PETCLINIC_WAR_URL
                  value: "${petclinic_war_url}"
              name: tomcat8
              image: "${tomcat_image}"
              imagePullPolicy: Always
              readinessProbe:
                exec:
                  command:
                    - /bin/bash
                    - '-c'
                    - > 
                    - curl -s -u admin:admin 'http://tomcat8:8080/manager/jmxproxy/?get=Catalina%3Atype%3DServer&att=stateName' | grep -iq 'stateName *= *STARTED'
              ports:
                - name: http
                  containerPort: 8080
                  protocol: TCP
          nodeSelector:
            node-id: node-1
      triggers:
        - type: ConfigChange
  - apiVersion: v1
    kind: DeploymentConfig
    metadata:
      creationTimestamp: null
      name: 'tomcat8-mysql'
      labels:
        app: "${app_name}"      
        project: "${project_name}"
    spec:
      replicas: 1
      selector:
        name: 'tomcat8-mysql'
      strategy:
        type: Recreate
      template:
        metadata:
          creationTimestamp: null
          labels:
            name: 'tomcat8-mysql'
        spec:
          serviceAccountName: alm-totta
          serviceAccount: alm-totta    
          containers:
            - capabilities: {}
              env:
                - name: MYSQL_USER
                  valueFrom:
                    secretKeyRef:
                      key: mysql_user
                      name: 'tomcat8-mysql'
                - name: MYSQL_PASSWORD
                  valueFrom:
                    secretKeyRef:
                      key: mysql_password
                      name: 'tomcat8-mysql'
                - name: MYSQL_ROOT_PASSWORD
                  valueFrom:
                    secretKeyRef:
                      key: mysql_root_password
                      name: 'tomcat8-mysql'
                - name: MYSQL_DATABASE
                  value: 'petclinic'
              image: "${mysql_image}"
              imagePullPolicy: IfNotPresent
              livenessProbe:
                initialDelaySeconds: 30
                tcpSocket:
                  port: 3306
                timeoutSeconds: 1
              name: mysql
              ports:
                - containerPort: 3306
                  protocol: TCP
              readinessProbe:
                exec:
                  command:
                    - /bin/sh
                    - '-i'
                    - '-c'
                    - >-
                      MYSQL_PWD="$MYSQL_PASSWORD" mysql -h 127.0.0.1 -u
                      $MYSQL_USER -D $MYSQL_DATABASE -e 'SELECT 1'
                initialDelaySeconds: 5
                timeoutSeconds: 1
              resources:
                limits:
                  memory: '512Mi'
              securityContext:
                capabilities: {}
                privileged: false
              terminationMessagePath: /dev/termination-log
              volumeMounts:
                - mountPath: /var/lib/mysql/data
                  name: 'tomcat8-mysql-data'
          dnsPolicy: ClusterFirst
          restartPolicy: Always
          nodeSelector:
            node-id: node-1
          volumes:
            - emptyDir:
                medium: ''
              name: 'tomcat8-mysql-data'
      triggers:
        - type: ConfigChange
    status: {}   
  - apiVersion: v1
    kind: Route
    id: tomcat8-http
    metadata:
      annotations:
        description: Route for application's http service.
      labels:
        app: "${app_name}"    
        project: "${project_name}"
      name: tomcat8
    spec:
      to:
        name: tomcat8
      tls:
        termination: edge
        insecureEdgeTerminationPolicy: Redirect    
      path: '/petclinic'   
  - apiVersion: v1
    kind: Secret
    metadata:
      name: "tomcat8-mysql"
      labels:
        app: "tomcat8"
        project: "${project_name}"
    stringData:
      mysql_password: "${mysql_password}"
      mysql_root_password: "${mysql_root_password}"
      mysql_user: "${mysql_user}"    
  - apiVersion: v1
    kind: Service
    metadata:
      creationTimestamp: null
      name: 'tomcat8-mysql'
      labels:
        app: "${app_name}"        
        project: "${project_name}"
    spec:
      ports:
        - name: mysql
          nodePort: 0
          port: 3306
          protocol: TCP
          targetPort: 3306
      selector:
        name: 'tomcat8-mysql'
      sessionAffinity: None
      type: ClusterIP
    status:
      loadBalancer: {}             
  - apiVersion: v1
    kind: Service
    spec:
      ports:
        - port: 8080
          targetPort: 8080
      selector:
        deploymentConfig: tomcat8
    metadata:
      name: tomcat8
      labels:
        app: "${app_name}"
        project: "${project_name}"
      annotations:
        description: The web server's http port.
        service.alpha.openshift.io/dependencies: '[{"name":"tomcat8-mysql","namespace":"","kind":"Service"}]'
parameters:
  - name: project_name
    displayName: Project Name
    description: Project Name
    value: "petclinic-cloud"
  - name: app_name
    displayName: Application Name
    description: Application Name
    value: "petclinic"    
    required: true
  - name: petclinic_war_url
    displayName: Petclinic War URL
    description: Petclinic War URL    
  - name: tomcat_image
    displayName: Tomcat Image
    description: Tomcat Image
    required: true    
  - name: mysql_image
    displayName: MySQL Image
    description: MySQL Image
    value: centos/mysql-57-centos7
    required: true        
  - name: mysql_user
    displayName: MySQL Database User
    description: MySQL Database User
    value: petclinic
    required: true        
  - name: mysql_password
    displayName: MySQL Database Password
    description: MySQL Database Password
    required: true        
  - name: mysql_root_password
    displayName: MySQL Database Root Password
    description: MySQL Database Root Password
    required: true           
                     